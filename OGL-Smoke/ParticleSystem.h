#pragma once
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <algorithm>
#include "Camera.h"

using namespace std;
using namespace glm;

struct Particle
{
    vec3 pos, speed;
    float life;
    float cameradistance;

    bool operator<(const Particle& that) const
    {
        return this->cameradistance > that.cameradistance;
    }
};

class ParticleSystem
{
private:
    int LastUsedParticle;
    Particle *ParticlesContainer;

    int FindUnusedParticle()
    {
        for (int i = LastUsedParticle; i < MaxParticles; i++) {
            if (ParticlesContainer[i].life < 0) {
                LastUsedParticle = i;
                return i;
            }
        }

        for (int i = 0; i < LastUsedParticle; i++) {
            if (ParticlesContainer[i].life < 0) {
                LastUsedParticle = i;
                return i;
            }
        }
        return 0;
    }

    void SortParticles()
    {
        sort(&ParticlesContainer[0], &ParticlesContainer[MaxParticles]);
    }

    int GetNumberParticles(float dt)
    {
        int newparticles = (int)(dt * 20000.0);
        if (newparticles > (int)(0.016f * 20000.0))
            newparticles = (int)(0.016f * 20000.0);
        return newparticles;
    }

    void SetupParticleSystem(float dt, float Spread, float Life, vec3 Position, vec3 InitialVelocity)
    {
        for (int i = 0; i < GetNumberParticles(dt); i++)
        {
            int particleIndex = FindUnusedParticle();
            ParticlesContainer[particleIndex].life = Life;
            ParticlesContainer[particleIndex].pos = Position;

            vec3 randomdir = vec3(
                (rand() % 2000 - 1000.0f) / 1000.0f,
                (rand() % 2000 - 1000.0f) / 1000.0f,
                (rand() % 2000 - 1000.0f) / 1000.0f
            );

            ParticlesContainer[particleIndex].speed = InitialVelocity + randomdir * Spread;
        }
    }
public:
    float* Particule_Positions;
    int ParticlesCount;
    int MaxParticles;
    float ParticleSize;

    ParticleSystem(int MaxParticleCount, float pSize)
    {
        MaxParticles = MaxParticleCount;
        ParticleSize = pSize;
        LastUsedParticle = 0;
        ParticlesCount = 0;
        ParticlesContainer = new Particle[MaxParticleCount];

        Particule_Positions = new float[MaxParticles * 3];

        for (int i = 0; i < MaxParticles; i++)
        {
            ParticlesContainer[i].life = -1.0f;
            ParticlesContainer[i].cameradistance = -1.0f;
        }
    }

    void UpdateSystem(float dt, float Spread, float Life, vec3 Gravity, vec3 InitialVelocity, vec3 ExternalForce, vec3 EmitterPosition, Camera *Cam)
    {
        SetupParticleSystem(dt, Spread, Life, EmitterPosition, InitialVelocity);

        ParticlesCount = 0;

        for (int i = 0; i < MaxParticles; i++)
        {
            Particle& p = ParticlesContainer[i];

            if (p.life > 0.0f)
            {
                p.life -= dt;
                if (p.life > 0.0f)
                {
                    p.speed += (Gravity + ExternalForce) * (float)dt * 0.5f;
                    p.pos += p.speed * (float)dt;
                    p.cameradistance = length(p.pos - Cam->GetPosition());

                    Particule_Positions[3 * ParticlesCount + 0] = p.pos.x;
                    Particule_Positions[3 * ParticlesCount + 1] = p.pos.y;
                    Particule_Positions[3 * ParticlesCount + 2] = p.pos.z;
                }
                else
                    p.cameradistance = -1.0f;

                ParticlesCount++;
            }
        }

        SortParticles();
    }

    ~ParticleSystem()
    {
        if (Particule_Positions)
            delete Particule_Positions;
        if (ParticlesContainer)
            delete ParticlesContainer;
    }
};