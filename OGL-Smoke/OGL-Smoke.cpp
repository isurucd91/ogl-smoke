#include <glad\glad.h> 
#include <GLFW\glfw3.h>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <iostream>
#include "TextureManager.h"
#include "ShaderManager.h"
#include "Camera.h"
#include "ParticleSystem.h"

using namespace std;

void FamebuffersizeCallback(GLFWwindow* window, int width, int height);
void HandleInput(GLFWwindow* window);
void MouseCallback(GLFWwindow* window, double xpos, double ypos);
void ScrollCallback(GLFWwindow* window, double xoffset, double yoffset);
void MouseDragCallback(GLFWwindow* window, int button, int action, int mods);

const float SCREEN_WIDTH = 800;
const float SCREEN_HEIGHT = 600;

float deltaTime = 0.0f;
float lastFrame = 0.0f;

Camera* Cam = NULL;
ParticleSystem* PS = NULL;

float lastX = 800 / 2;
float lastY = 600 / 2;
bool lbutton_down = false;

glm::vec3 EmitterPos = glm::vec3(0, 0, -10);

int main()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = glfwCreateWindow((int)SCREEN_WIDTH, (int)SCREEN_HEIGHT, "OPENGL-Smoke", NULL, NULL);
    if (window == NULL)
    {
        cout << "Failed to create GLFW window" << endl;
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, FamebuffersizeCallback);
    glfwSetCursorPosCallback(window, MouseCallback);
    glfwSetScrollCallback(window, ScrollCallback);
    glfwSetMouseButtonCallback(window, MouseDragCallback);

 //   glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        cout << "Failed to initialize GLAD" << endl;
        return -1;
    }

    glEnable(GL_DEPTH_TEST);

    ShaderManager *SM = new ShaderManager();

    if (SM->CompileNBindShader(GL_VERTEX_SHADER, "vs.glsl") == -1)
    {
        delete SM;
        glfwTerminate();
        return -1;
    }

    if (SM->CompileNBindShader(GL_GEOMETRY_SHADER, "gs.glsl") == -1)
    {
        delete SM;
        glfwTerminate();
        return -1;
    }

    if (SM->CompileNBindShader(GL_FRAGMENT_SHADER, "fs.glsl") == -1)
    {
        delete SM;
        glfwTerminate();
        return -1;
    }

    Cam = new Camera(SCREEN_WIDTH, SCREEN_HEIGHT, 45.0f, glm::vec3(0, 0, 3));

    PS = new ParticleSystem(1000, 0.5175f);//.175

    unsigned int VBO, VAO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(PS->Particule_Positions), PS->Particule_Positions, GL_STREAM_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

    TextureManager* tex1 = new TextureManager("1.png");

    tex1->BindTextureParamsToShader(SM->GetShaderProgramID(), "Texture1", 0);

    while (!glfwWindowShouldClose(window))
    {
        float currentFrame = (float)glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        HandleInput(window);

        glClearColor(0.2f, 0.3f, 0.5f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        float ang = (float)glfwGetTime() * glm::radians(70.0f);

        PS->UpdateSystem(deltaTime, 1.05f, 2.0f, vec3(0, -7.81, 0), vec3(0.0f, 6.5f, 0.0f), vec3(sin(ang), sin(ang), 0), EmitterPos, Cam);
  
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)PS->MaxParticles * 3 * sizeof(float), NULL, GL_STREAM_DRAW);
        glBufferSubData(GL_ARRAY_BUFFER, 0, PS->ParticlesCount * sizeof(float) * 3, PS->Particule_Positions);
        
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


        glm::mat4 model = glm::mat4(1.0f);
   //     model = glm::rotate(model, (float)glfwGetTime() * glm::radians(50.0f), glm::vec3(0.0f, 1.0f, 0.0f));

        glm::mat4 view;
        view = Cam->GetViewMatrix();

        glm::mat4 projection;
        projection = glm::perspective(glm::radians(Cam->GetZoom()), SCREEN_WIDTH / SCREEN_HEIGHT, 0.01f, 100.0f);


        SM->Run();
        SM->SetUniformMat4("model", model);
        SM->SetUniformMat4("view", view);
        SM->SetUniformMat4("projection", projection);
        SM->SetUniformFloat("size", PS->ParticleSize);

        tex1->ActivateTexture(GL_TEXTURE0);

        glBindVertexArray(VAO);
        glDrawArrays(GL_POINTS, 0, PS->ParticlesCount);
        glBindVertexArray(0);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    delete PS;
    delete SM;
    delete tex1;
    delete Cam;
    glfwTerminate();
    return 0;
}

void HandleInput(GLFWwindow* window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        Cam->KeyBoardInputforCamera(FORWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        Cam->KeyBoardInputforCamera(BACKWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        Cam->KeyBoardInputforCamera(LEFT, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        Cam->KeyBoardInputforCamera(RIGHT, deltaTime);

    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
        EmitterPos += glm::vec3(0, 0, 1) * deltaTime;
    if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
        EmitterPos -= glm::vec3(0, 0, 1) * deltaTime;
    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
        EmitterPos -= glm::vec3(1, 0, 0) * deltaTime;
    if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
        EmitterPos += glm::vec3(1, 0, 0) * deltaTime;
}

void FamebuffersizeCallback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}

void MouseCallback(GLFWwindow* window, double xpos, double ypos)
{
    Cam->MouseMoveInputforCamera((float)xpos, (float)ypos);
}

void ScrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
    Cam->MouseScrollInputforCamera((float)yoffset);

}

static void MouseDragCallback(GLFWwindow* window, int button, int action, int mods)
{
    double xpos, ypos;

    if (button == GLFW_MOUSE_BUTTON_LEFT)
    { 
        glfwGetCursorPos(window, &xpos, &ypos);

        if (GLFW_PRESS == action)
            lbutton_down = true;
        else if (GLFW_RELEASE == action)
        {
            lbutton_down = false;
            lastX = xpos;
            lastY = ypos;
        }
    }
    float xoffset=0, yoffset=0;
    if (lbutton_down)
    {
        xoffset = xpos - lastX;
        yoffset = lastY - ypos;
    } 
}