#pragma once
#include <glad\glad.h> 
#include <GLFW\glfw3.h>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

using namespace std;

class ShaderManager
{
private:
	unsigned int ShaderProgram;

public:
	ShaderManager()
	{
		ShaderProgram = glCreateProgram();
	}

	int CompileNBindShader(GLenum ShaderType, string fName)
	{
		fstream ShaderFile;
		ShaderFile.open(fName, ios::in);
		stringstream shadercode;
		shadercode << ShaderFile.rdbuf();
		ShaderFile.close();

		string Code = shadercode.str();
		const char* ShaderCode = Code.c_str();

		unsigned int ShaderID = glCreateShader(ShaderType);
		glShaderSource(ShaderID, 1, &ShaderCode, NULL);
		glCompileShader(ShaderID);

		int success;
		char infoLog[512];
		glGetShaderiv(ShaderID, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(ShaderID, 512, NULL, infoLog);
			cout << infoLog << endl;
			glDeleteShader(ShaderID);
			return -1;
		}

		glAttachShader(ShaderProgram, ShaderID);
		glLinkProgram(ShaderProgram);

		glGetProgramiv(ShaderProgram, GL_LINK_STATUS, &success);
		if (!success)
		{
			glGetProgramInfoLog(ShaderProgram, 512, NULL, infoLog);
			cout << infoLog << endl;
			glDeleteShader(ShaderID);
			return -1;
		}

		glDeleteShader(ShaderID);
		return 0;
	}

	void SetUniformMat4(string Param, glm::mat4 Value)
	{
		glUniformMatrix4fv(glGetUniformLocation(ShaderProgram, Param.c_str()), 1, GL_FALSE, glm::value_ptr(Value));
	}

	void SetUniformVec3(string Param, glm::vec3 Value)
	{
		glUniform3fv(glGetUniformLocation(ShaderProgram, Param.c_str()), 1, glm::value_ptr(Value));
	}

	void SetUniformVec4(string Param, glm::vec4 Value)
	{
		glUniform4fv(glGetUniformLocation(ShaderProgram, Param.c_str()), 1, glm::value_ptr(Value));
	}

	void SetUniformFloat(string Param, float Value)
	{
		glUniform1f(glGetUniformLocation(ShaderProgram, Param.c_str()), Value);
	}

	unsigned int GetShaderProgramID()
	{
		return ShaderProgram;
	}

	void Run()
	{
		glUseProgram(ShaderProgram);
	}

	~ShaderManager()
	{
		glDeleteProgram(ShaderProgram);
	}
};
