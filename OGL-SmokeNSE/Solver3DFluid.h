
// Based on Jos Stam, "Real-Time Fluid Dynamics for Games"

#pragma once
#include <iostream>
#include <time.h>
#include <algorithm>
#include <vector>

#define IX(i,j,k) ((i)+(M+2)*(j) + (M+2)*(N+2)*(k))
#define SWAP(x0,x) {float * tmp=x0;x0=x;x=tmp;}
#define MAX(a,b)            (((a) > (b)) ? (a) : (b))
#define LINEARSOLVERTIMES 10
#define SIZE 40

using namespace std;

struct Vertex
{
    glm::vec3 pos;
    glm::vec4 color;
};

class Solver3DFluid
{
private:
    int M; // grid x
    int N; // grid y
    int O; // grid z
    float diff; // diffuse
    float visc; // viscosity
    float force;  // added on keypress on an axis
    float source; // density

    void add_source(int M, int N, int O, float* x, float* s, float dt)
    {
        int i, size = (M + 2) * (N + 2) * (O + 2);
        for (i = 0; i < size; i++) x[i] += dt * s[i];
    }

    void  set_bnd(int M, int N, int O, int b, float* x)
    {
        int i, j;

        //setting faces
        for (i = 1; i <= M; i++) {
            for (j = 1; j <= N; j++) {
                x[IX(i, j, 0)] = b == 3 ? -x[IX(i, j, 1)] : x[IX(i, j, 1)];
                x[IX(i, j, O + 1)] = b == 3 ? -x[IX(i, j, O)] : x[IX(i, j, O)];
            }
        }

        for (i = 1; i <= N; i++) {
            for (j = 1; j <= O; j++) {
                x[IX(0, i, j)] = b == 1 ? -x[IX(1, i, j)] : x[IX(1, i, j)];
                x[IX(M + 1, i, j)] = b == 1 ? -x[IX(M, i, j)] : x[IX(M, i, j)];
            }
        }

        for (i = 1; i <= M; i++) {
            for (j = 1; j <= O; j++) {
                x[IX(i, 0, j)] = b == 2 ? -x[IX(i, 1, j)] : x[IX(i, 1, j)];
                x[IX(i, N + 1, j)] = b == 2 ? -x[IX(i, N, j)] : x[IX(i, N, j)];
            }
        }

        //Setting edges
        for (i = 1; i <= M; i++) {
            x[IX(i, 0, 0)] = 1.0 / 2.0 * (x[IX(i, 1, 0)] + x[IX(i, 0, 1)]);
            x[IX(i, N + 1, 0)] = 1.0 / 2.0 * (x[IX(i, N, 0)] + x[IX(i, N + 1, 1)]);
            x[IX(i, 0, O + 1)] = 1.0 / 2.0 * (x[IX(i, 0, O)] + x[IX(i, 1, O + 1)]);
            x[IX(i, N + 1, O + 1)] = 1.0 / 2.0 * (x[IX(i, N, O + 1)] + x[IX(i, N + 1, O)]);
        }

        for (i = 1; i <= N; i++) {
            x[IX(0, i, 0)] = 1.0 / 2.0 * (x[IX(1, i, 0)] + x[IX(0, i, 1)]);
            x[IX(M + 1, i, 0)] = 1.0 / 2.0 * (x[IX(M, i, 0)] + x[IX(M + 1, i, 1)]);
            x[IX(0, i, O + 1)] = 1.0 / 2.0 * (x[IX(0, i, O)] + x[IX(1, i, O + 1)]);
            x[IX(M + 1, i, O + 1)] = 1.0 / 2.0 * (x[IX(M, i, O + 1)] + x[IX(M + 1, i, O)]);
        }

        for (i = 1; i <= O; i++) {
            x[IX(0, 0, i)] = 1.0 / 2.0 * (x[IX(0, 1, i)] + x[IX(1, 0, i)]);
            x[IX(0, N + 1, i)] = 1.0 / 2.0 * (x[IX(0, N, i)] + x[IX(1, N + 1, i)]);
            x[IX(M + 1, 0, i)] = 1.0 / 2.0 * (x[IX(M, 0, i)] + x[IX(M + 1, 1, i)]);
            x[IX(M + 1, N + 1, i)] = 1.0 / 2.0 * (x[IX(M + 1, N, i)] + x[IX(M, N + 1, i)]);
        }

        //setting corners
        x[IX(0, 0, 0)] = 1.0 / 3.0 * (x[IX(1, 0, 0)] + x[IX(0, 1, 0)] + x[IX(0, 0, 1)]);
        x[IX(0, N + 1, 0)] = 1.0 / 3.0 * (x[IX(1, N + 1, 0)] + x[IX(0, N, 0)] + x[IX(0, N + 1, 1)]);

        x[IX(M + 1, 0, 0)] = 1.0 / 3.0 * (x[IX(M, 0, 0)] + x[IX(M + 1, 1, 0)] + x[IX(M + 1, 0, 1)]);
        x[IX(M + 1, N + 1, 0)] = 1.0 / 3.0 * (x[IX(M, N + 1, 0)] + x[IX(M + 1, N, 0)] + x[IX(M + 1, N + 1, 1)]);

        x[IX(0, 0, O + 1)] = 1.0 / 3.0 * (x[IX(1, 0, O + 1)] + x[IX(0, 1, O + 1)] + x[IX(0, 0, O)]);
        x[IX(0, N + 1, O + 1)] = 1.0 / 3.0 * (x[IX(1, N + 1, O + 1)] + x[IX(0, N, O + 1)] + x[IX(0, N + 1, O)]);

        x[IX(M + 1, 0, O + 1)] = 1.0 / 3.0 * (x[IX(M, 0, O + 1)] + x[IX(M + 1, 1, O + 1)] + x[IX(M + 1, 0, O)]);
        x[IX(M + 1, N + 1, O + 1)] = 1.0 / 3.0 * (x[IX(M, N + 1, O + 1)] + x[IX(M + 1, N, O + 1)] + x[IX(M + 1, N + 1, O)]);
    }

    void lin_solve(int M, int N, int O, int b, float* x, float* x0, float a, float c)
    {
        int i, j, k, l;

        for (l = 0; l < LINEARSOLVERTIMES; l++) {
            for (i = 1; i <= M; i++) {
                for (j = 1; j <= N; j++) {
                    for (k = 1; k <= O; k++) {
                        x[IX(i, j, k)] = (x0[IX(i, j, k)] + a * (x[IX(i - 1, j, k)] + x[IX(i + 1, j, k)] + x[IX(i, j - 1, k)] + x[IX(i, j + 1, k)] + x[IX(i, j, k - 1)] + x[IX(i, j, k + 1)])) / c;
                    }
                }
            }
            set_bnd(M, N, O, b, x);
        }
    }

    void diffuse(int M, int N, int O, int b, float* x, float* x0, float diff, float dt)
    {
        int max = MAX(MAX(M, N), MAX(N, O));
        float a = dt * diff * max * max * max;
        lin_solve(M, N, O, b, x, x0, a, 1 + 6 * a);
    }

    void advect(int M, int N, int O, int b, float* d, float* d0, float* u, float* v, float* w, float dt)
    {
        int i, j, k, i0, j0, k0, i1, j1, k1;
        float x, y, z, s0, t0, s1, t1, u1, u0, dtx, dty, dtz;

        dtx = dty = dtz = dt * MAX(MAX(M, N), MAX(N, O));

        for (i = 1; i <= M; i++) {
            for (j = 1; j <= N; j++) {
                for (k = 1; k <= O; k++) {
                    x = i - dtx * u[IX(i, j, k)]; y = j - dty * v[IX(i, j, k)]; z = k - dtz * w[IX(i, j, k)];
                    if (x < 0.5f) x = 0.5f; if (x > M + 0.5f) x = M + 0.5f; i0 = (int)x; i1 = i0 + 1;
                    if (y < 0.5f) y = 0.5f; if (y > N + 0.5f) y = N + 0.5f; j0 = (int)y; j1 = j0 + 1;
                    if (z < 0.5f) z = 0.5f; if (z > O + 0.5f) z = O + 0.5f; k0 = (int)z; k1 = k0 + 1;

                    s1 = x - i0; s0 = 1 - s1; t1 = y - j0; t0 = 1 - t1; u1 = z - k0; u0 = 1 - u1;
                    d[IX(i, j, k)] = s0 * (t0 * u0 * d0[IX(i0, j0, k0)] + t1 * u0 * d0[IX(i0, j1, k0)] + t0 * u1 * d0[IX(i0, j0, k1)] + t1 * u1 * d0[IX(i0, j1, k1)]) +
                        s1 * (t0 * u0 * d0[IX(i1, j0, k0)] + t1 * u0 * d0[IX(i1, j1, k0)] + t0 * u1 * d0[IX(i1, j0, k1)] + t1 * u1 * d0[IX(i1, j1, k1)]);
                }
            }
        }

        set_bnd(M, N, O, b, d);
    }

    void project(int M, int N, int O, float* u, float* v, float* w, float* p, float* div)
    {
        int i, j, k;

        for (i = 1; i <= M; i++) {
            for (j = 1; j <= N; j++) {
                for (k = 1; k <= O; k++) {
                    div[IX(i, j, k)] = -1.0 / 3.0 * ((u[IX(i + 1, j, k)] - u[IX(i - 1, j, k)]) / M + (v[IX(i, j + 1, k)] - v[IX(i, j - 1, k)]) / M + (w[IX(i, j, k + 1)] - w[IX(i, j, k - 1)]) / M);
                    p[IX(i, j, k)] = 0;
                }
            }
        }

        set_bnd(M, N, O, 0, div); set_bnd(M, N, O, 0, p);

        lin_solve(M, N, O, 0, p, div, 1, 6);

        for (i = 1; i <= M; i++) {
            for (j = 1; j <= N; j++) {
                for (k = 1; k <= O; k++) {
                    u[IX(i, j, k)] -= 0.5f * M * (p[IX(i + 1, j, k)] - p[IX(i - 1, j, k)]);
                    v[IX(i, j, k)] -= 0.5f * M * (p[IX(i, j + 1, k)] - p[IX(i, j - 1, k)]);
                    w[IX(i, j, k)] -= 0.5f * M * (p[IX(i, j, k + 1)] - p[IX(i, j, k - 1)]);
                }
            }
        }

        set_bnd(M, N, O, 1, u); set_bnd(M, N, O, 2, v); set_bnd(M, N, O, 3, w);
    }

    void dens_step(int M, int N, int O, float* x, float* x0, float* u, float* v, float* w, float diff, float dt)
    {
        add_source(M, N, O, x, x0, dt);
        SWAP(x0, x); diffuse(M, N, O, 0, x, x0, diff, dt);
        SWAP(x0, x); advect(M, N, O, 0, x, x0, u, v, w, dt);
    }

    void vel_step(int M, int N, int O, float* u, float* v, float* w, float* u0, float* v0, float* w0, float visc, float dt)
    {
        add_source(M, N, O, u, u0, dt); add_source(M, N, O, v, v0, dt); add_source(M, N, O, w, w0, dt);
        SWAP(u0, u); diffuse(M, N, O, 1, u, u0, visc, dt);
        SWAP(v0, v); diffuse(M, N, O, 2, v, v0, visc, dt);
        SWAP(w0, w); diffuse(M, N, O, 3, w, w0, visc, dt);
        project(M, N, O, u, v, w, u0, v0);
        SWAP(u0, u); SWAP(v0, v); SWAP(w0, w);
        advect(M, N, O, 1, u, u0, u0, v0, w0, dt); advect(M, N, O, 2, v, v0, u0, v0, w0, dt); advect(M, N, O, 3, w, w0, u0, v0, w0, dt);
        project(M, N, O, u, v, w, u0, v0);
    }
    int sim_shutdown(void)
    {
        free_data();
        Vertices.clear();
        return 0;
    }

    void free_data(void)
    {
        if (u) free(u);
        if (v) free(v);
        if (w) free(w);
        if (u_prev) free(u_prev);
        if (v_prev) free(v_prev);
        if (w_prev) free(w_prev);
        if (dens) free(dens);
        if (dens_prev) free(dens_prev);
    }

    void clear_data(void)
    {
        int i, size = (M + 2) * (N + 2) * (O + 2);

        for (i = 0; i < size; i++) {
            u[i] = v[i] = w[i] = u_prev[i] = v_prev[i] = w_prev[i] = dens[i] = dens_prev[i] = 0.0f;
        }

        addforce[0] = addforce[1] = addforce[2] = 0;

        Vertices.clear();

        for (int i = 0; i < MaxParticles; i++)
        {
            Vertex vtx;
            vtx.pos = glm::vec3(0, 0, 0);
            vtx.color = glm::vec4(1, 1, 1, 1);
            Vertices.push_back(vtx);
        }

    }

    int allocate_data(void)
    {
        int size = (M + 2) * (N + 2) * (O + 2);

        u = (float*)malloc(size * sizeof(float));
        v = (float*)malloc(size * sizeof(float));
        w = (float*)malloc(size * sizeof(float));
        u_prev = (float*)malloc(size * sizeof(float));
        v_prev = (float*)malloc(size * sizeof(float));
        w_prev = (float*)malloc(size * sizeof(float));
        dens = (float*)malloc(size * sizeof(float));
        dens_prev = (float*)malloc(size * sizeof(float));

        if (!u || !v || !w || !u_prev || !v_prev || !w_prev || !dens || !dens_prev) {
            cout << "cannot allocate data" << endl;
            return (-1);
        }

        return (0);
    }

    void get_force_source(float* d, float* u, float* v, float* w)
    {
        int i, j, k, size = (M + 2) * (N + 2) * (O + 2);;

        for (i = 0; i < size; i++) {
            u[i] = v[i] = w[i] = d[i] = 0.0f;
        }

        if (addforce[0] == 1) // x
        {
            i = 2,
            j = N / 2;
            k = O / 2;

            if (i<1 || i>M || j<1 || j>N || k <1 || k>O) return;
            u[IX(i, j, k)] = force * 10;
            addforce[0] = 0;
        }

        if (addforce[1] == 1)//y
        {
            i = M / 2,
            j = 2;
            k = O / 2;

            if (i<1 || i>M || j<1 || j>N || k <1 || k>O) return;
            v[IX(i, j, k)] = force * 10;
            addforce[1] = 0;
        }

        if (addforce[2] == 1) // z
        {
            i = M / 2,
            j = N / 2;
            k = 2;

            if (i<1 || i>M || j<1 || j>N || k <1 || k>O) return;
            w[IX(i, j, k)] = force * 10;
            addforce[2] = 0;
        }

        if (addsource == 1)
        {
            i = M / 2,
            j = N / 2;
            k = O / 2;
            d[IX(i, j, k)] = source;
            addsource = 0;
        }
        return;
    }
public:
    int addforce[3];
    int addsource;

    float* u, * v, * w, * u_prev, * v_prev, * w_prev;
    float* dens, * dens_prev;

    int MaxParticles;
    vector<Vertex> Vertices;

    Solver3DFluid()
    {
        M = SIZE; // grid x
        N = SIZE; // grid y
        O = SIZE; // grid z
        MaxParticles = (M + 2) * (N + 2) * (O + 2);
        diff = 0.001f; // diffuse
        visc = 0.00000f; // viscosity
        force = 10.0f;  // added on keypress on an each axis
        source = 200.0f; // density
        addforce[0] = 0;
        addforce[1] = 0;
        addforce[2] = 0;
        addsource = 1;

        allocate_data();
        clear_data();
    }

    ~Solver3DFluid()
    {
        sim_shutdown();
    }

    void sim_reset()
    {
        clear_data();
    }

    void simulate(float dt)
    {
        get_force_source(dens_prev, u_prev, v_prev, w_prev);
        vel_step(M, N, O, u, v, w, u_prev, v_prev, w_prev, visc, dt);
        dens_step(M, N, O, dens, dens_prev, u, v, w, diff, dt);
    }
};


