#pragma once
#include <glad\glad.h> 
#include <GLFW\glfw3.h>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>

enum Movement
{
	FORWARD = 0,
	BACKWARD = 1,
	LEFT = 2,
	RIGHT = 3
};


class Camera
{
private:
	glm::vec3 cameraPos;
	glm::vec3 cameraFront;
	glm::vec3 cameraUp;
	bool firstMouse;
	float yaw;
	float pitch;
	float lastX;
	float lastY;
	float fov;
	float FOV;

public:
	Camera(float Scr_Width, float Scr_Height, float CamFOV, glm::vec3 CamPos)
	{
		cameraPos = CamPos;
		cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
		cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);

		firstMouse = true;
		yaw = -90.0f;
		pitch = 0.0f;
		lastX = Scr_Width / 2.0f;
		lastY = Scr_Height / 2.0f;
		fov = CamFOV;
		FOV = CamFOV;
	}

	glm::mat4 GetViewMatrix()
	{
		return glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
	}

	void KeyBoardInputforCamera(Movement CamMove, float dTime)
	{
		float cameraSpeed = 2.5f * dTime;
		if (CamMove == FORWARD)
			cameraPos += cameraSpeed * cameraFront;
		if (CamMove == BACKWARD)
			cameraPos -= cameraSpeed * cameraFront;
		if (CamMove == LEFT)
			cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
		if (CamMove == RIGHT)
			cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
	}

	float GetZoom()
	{
		return fov;
	}

	glm::vec3 GetPosition()
	{
		return cameraPos;
	}

	void MouseMoveInputforCamera(float xpos, float ypos)
	{
		if (firstMouse)
		{
			lastX = xpos;
			lastY = ypos;
			firstMouse = false;
		}

		float xoffset = xpos - lastX;
		float yoffset = lastY - ypos;
		lastX = xpos;
		lastY = ypos;

		float sensitivity = 0.1f;
		xoffset *= sensitivity;
		yoffset *= sensitivity;

		yaw += xoffset;
		pitch += yoffset;

		if (pitch > 89.0f)
			pitch = 89.0f;
		if (pitch < -89.0f)
			pitch = -89.0f;

		glm::vec3 front;
		front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
		front.y = sin(glm::radians(pitch));
		front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
		cameraFront = glm::normalize(front);
	}

	void MouseScrollInputforCamera(float yoffset)
	{
		fov -= (float)yoffset;
		if (fov < 1.0f)
			fov = 1.0f;
		if (fov > FOV)
			fov = FOV;
	}
};