#include <glad\glad.h> 
#include <GLFW\glfw3.h>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <iostream>

#include "TextureManager.h"
#include "ShaderManager.h"
#include "Camera.h"
#include "Solver3DFluid.h"

using namespace std;

void FamebuffersizeCallback(GLFWwindow* window, int width, int height);
void HandleInput(GLFWwindow* window);
void MouseCallback(GLFWwindow* window, double xpos, double ypos);
void ScrollCallback(GLFWwindow* window, double xoffset, double yoffset);
void MouseDragCallback(GLFWwindow* window, int button, int action, int mods);

const float SCREEN_WIDTH = 800;
const float SCREEN_HEIGHT = 600;

float deltaTime = 0.0f;
float lastFrame = 0.0f;

Camera* Cam = NULL;

float lastX = 800 / 2;
float lastY = 600 / 2;
bool lbutton_down = false;

Solver3DFluid* S3DF = NULL;

int main()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = glfwCreateWindow((int)SCREEN_WIDTH, (int)SCREEN_HEIGHT, "OPENGL-SmokeNSE", NULL, NULL);
    if (window == NULL)
    {
        cout << "Failed to create GLFW window" << endl;
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, FamebuffersizeCallback);
    glfwSetCursorPosCallback(window, MouseCallback);
    glfwSetScrollCallback(window, ScrollCallback);
    glfwSetMouseButtonCallback(window, MouseDragCallback);

 //   glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        cout << "Failed to initialize GLAD" << endl;
        return -1;
    }

    glEnable(GL_DEPTH_TEST);

    ShaderManager *SM = new ShaderManager();

    if (SM->CompileNBindShader(GL_VERTEX_SHADER, "vs.glsl") == -1)
    {
        delete SM;
        glfwTerminate();
        return -1;
    }

    if (SM->CompileNBindShader(GL_FRAGMENT_SHADER, "fs.glsl") == -1)
    {
        delete SM;
        glfwTerminate();
        return -1;
    }

    Cam = new Camera(SCREEN_WIDTH, SCREEN_HEIGHT, 45.0f, glm::vec3(0, 0, 3));

    S3DF = new Solver3DFluid();

    unsigned int VBO, VAO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, S3DF->Vertices.size() * sizeof(Vertex), &S3DF->Vertices[0], GL_STREAM_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(glm::vec3)));

    while (!glfwWindowShouldClose(window))
    {
        float currentFrame = (float)glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        HandleInput(window);

        glClearColor(0.2f, 0.3f, 0.5f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  
        S3DF->simulate(deltaTime);

        glm::mat4 model = glm::mat4(1.0f);
   //     model = glm::rotate(model, (float)glfwGetTime() * glm::radians(50.0f), glm::vec3(0.0f, 1.0f, 0.0f));

        glm::mat4 view;
        view = Cam->GetViewMatrix();

        glm::mat4 projection;
        projection = glm::perspective(glm::radians(Cam->GetZoom()), SCREEN_WIDTH / SCREEN_HEIGHT, 0.01f, 100.0f);

        SM->Run();
        SM->SetUniformMat4("model", model);
        SM->SetUniformMat4("view", view);
        SM->SetUniformMat4("projection", projection);

        for (int i = 0; i < S3DF->MaxParticles; i++)
        {
            S3DF->Vertices[i].pos += glm::vec3(S3DF->u[i], S3DF->v[i], S3DF->w[i])* deltaTime;
            S3DF->Vertices[i].color = glm::vec4(S3DF->dens[i], S3DF->dens[i], S3DF->dens[i], 0.3f);
        }
        
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, S3DF->Vertices.size() * sizeof(Vertex), NULL, GL_STREAM_DRAW);
        glBufferSubData(GL_ARRAY_BUFFER, 0, S3DF->Vertices.size() * sizeof(Vertex), &S3DF->Vertices[0]);
        
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glBindVertexArray(VAO);
        glPointSize(3);
        glDrawArrays(GL_POINTS, 0, S3DF->Vertices.size());
        glBindVertexArray(0);
        
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    delete S3DF;
    delete SM;
    delete Cam;
    glfwTerminate();
    return 0;
}

void HandleInput(GLFWwindow* window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        Cam->KeyBoardInputforCamera(FORWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        Cam->KeyBoardInputforCamera(BACKWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        Cam->KeyBoardInputforCamera(LEFT, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        Cam->KeyBoardInputforCamera(RIGHT, deltaTime);

    if (glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS)
        S3DF->addforce[0] = 1;
    if (glfwGetKey(window, GLFW_KEY_Y) == GLFW_PRESS)
        S3DF->addforce[1] = 1;
    if (glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS)
        S3DF->addforce[2] = 1;
    if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS)
        S3DF->sim_reset();
    if (glfwGetKey(window, GLFW_KEY_V) == GLFW_PRESS)
        S3DF->addsource = 1;
}

void FamebuffersizeCallback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}

void MouseCallback(GLFWwindow* window, double xpos, double ypos)
{
    Cam->MouseMoveInputforCamera((float)xpos, (float)ypos);
}

void ScrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
    Cam->MouseScrollInputforCamera((float)yoffset);

}

static void MouseDragCallback(GLFWwindow* window, int button, int action, int mods)
{
    double xpos, ypos;

    if (button == GLFW_MOUSE_BUTTON_LEFT)
    { 
        glfwGetCursorPos(window, &xpos, &ypos);

        if (GLFW_PRESS == action)
            lbutton_down = true;
        else if (GLFW_RELEASE == action)
        {
            lbutton_down = false;
            lastX = xpos;
            lastY = ypos;
        }
    }
    float xoffset=0, yoffset=0;
    if (lbutton_down)
    {
        xoffset = xpos - lastX;
        yoffset = lastY - ypos;
    } 
}