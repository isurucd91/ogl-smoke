#pragma once
#include <glad\glad.h> 
#include <GLFW\glfw3.h>
#include <iostream>
#include <string>
#include "stb_image.h"

using namespace std;

class TextureManager
{
private:
        unsigned int Texture;
public:
	TextureManager(string fName)
	{
        stbi_set_flip_vertically_on_load(true);

        glGenTextures(1, &Texture);

        int width, height, nrChannels;
        unsigned char* data = stbi_load(fName.c_str(), &width, &height, &nrChannels, 0);
        if (data)
        {
            GLenum format;
            if (nrChannels == 3)
                format = GL_RGB;
            else if (nrChannels == 4)
                format = GL_RGBA;

            glBindTexture(GL_TEXTURE_2D, Texture);
            glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
            glGenerateMipmap(GL_TEXTURE_2D);

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        }
        else
        {
            cout << "Failed to load texture" << endl;
        }
        stbi_image_free(data);
	}

    void BindTextureParamsToShader(int ShaderProgram, const char *ParamName, int Index)
    {
        glUseProgram(ShaderProgram);
        glUniform1i(glGetUniformLocation(ShaderProgram, ParamName), Index);
    }

    void ActivateTexture(GLenum TextureParam)
    {
        glActiveTexture(TextureParam);
        glBindTexture(GL_TEXTURE_2D, Texture);
    }
};