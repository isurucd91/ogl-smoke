Smoke Simulation using Particle System

Introduction:
General particle system based on simple physics

![](1.gif)

Setup:
Extract OpenGL folder of OpenGL.rar to C:\Program Files (x86)

Use Visual Studio 2019

Controls:
W,S,A,D and Mouse Pointer - Camera controls
Up, Down, Left, Right - Emitter Position

========================================================================

Smoke Simulation using Navier Stokes Equations

Introduction:
A general 3D NSE solver which controls the particle system

![](2.gif)

Setup:
Extract OpenGL folder of OpenGL.rar to C:\Program Files (x86)

Use Visual Studio 2019

Controls:
W,S,A,D and Mouse Pointer - Camera controls
C - Reset
V - Add source
X,Y,Z - Add force in each direction
